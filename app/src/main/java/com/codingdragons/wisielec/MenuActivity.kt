package com.codingdragons.wisielec

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_menu.*

class MenuActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
        supportActionBar?.hide()

        ArrayAdapter.createFromResource(
            this,
            R.array.categories,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinnerCategory.setAdapter(adapter)
        }

        startButton.setOnClickListener { startGame() }
    }

    private fun getCategoryName() : String  {
        return resources.getStringArray(R.array.categories_values)[spinnerCategory.selectedIndex]
    }

    private fun startGame(){
        val intent = Intent(this, MainActivity::class.java)
        intent.putExtra("category", getCategoryName())
        startActivity(intent)
    }

}