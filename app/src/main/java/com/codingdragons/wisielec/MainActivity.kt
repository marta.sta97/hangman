package com.codingdragons.wisielec

import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.random.Random
import kotlin.random.nextInt

class MainActivity : AppCompatActivity() {

    private lateinit var word: String
    private lateinit var words: Array<String>
    private var round = 0
    private var buttons = arrayOf<Button>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.hide()

        val category = intent.getStringExtra("category")
        words = resources.getStringArray(resources.getIdentifier(category, "array", packageName))
        setNewGame()

        restartButton.setOnClickListener { restart() }
    }

    private fun setNewGame() {
        imageView.setImageResource(R.drawable.wisielec0)
        val r = Random.nextInt(0 until words.size)
        word = words[r].toLowerCase()
        hashWord()
    }

    fun getLetter(view: View) {
        val letter = view as Button
        if (round < 10) {
            buttons += letter
            if (word.contains(letter.text, true)) {
                letter.setTextColor(getColor(R.color.colorRightGuess))
                revealLetter(letter.text.toString())
            } else {
                letter.setTextColor(getColor(R.color.colorWrongGuess))
                round++
                if (round == 10) gameOver()
                else imageView.setImageResource(resources.getIdentifier("wisielec$round", "drawable", packageName))
            }
        }
    }

    private fun revealLetter(letter: String) {
        word = word.replace(letter.toLowerCase(), letter.toUpperCase())
        hashWord()
    }

    private fun hashWord() {
        var wordHashed = ""

        for (letter in word) {
            wordHashed += if (letter.isLowerCase()) {
                "_ "
            } else {
                "$letter "
            }
        }

        textView.text = wordHashed

        if (!wordHashed.contains("_")) {
            round = 10
            textView.setTextColor(getColor(R.color.colorRightGuess))
            Toast.makeText(this, getString(R.string.win), Toast.LENGTH_SHORT).show()
        }
    }

    private fun restart() {
        setNewGame()
        round = 0
        buttons.forEach { b -> b.setTextColor(Color.BLACK) }
        textView.setTextColor(Color.WHITE)
    }

    private fun gameOver() {
        imageView.setImageResource(R.drawable.wisielec10)
        Toast.makeText(this, getString(R.string.lose), Toast.LENGTH_SHORT).show()

        textView.text = word.toUpperCase()
        textView.setTextColor(getColor(R.color.colorWrongGuess))
    }

}
