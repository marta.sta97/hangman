# Hangman Mobile App

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Installation](#installation)

## General info
Polish version of the classic letter guessing game. 
You are shown a set of blank letters that match a word and you have to guess what these letters are.

## Technologies
Project is created with:
* Kotlin version: 1.3.21

## Installation
1. Download this project as zip and extract it
2. Import it in Android Studio
3. Sync Gradle and run on your device/emulator

